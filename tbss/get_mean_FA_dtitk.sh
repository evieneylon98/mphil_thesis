for bundle in AC AF_L AF_R AST_L AST_R C_L C_R CC_ForcepsMajor CC_ForcepsMinor CC CCMid CST_L CST_R F_L_R FPT_L FPT_R ICP_L ICP_R IFOF_L IFOF_R ILF_L ILF_R MCP MdLF_L MdLF_R ML_L ML_R OPT_L OPT_R OR_L OR_R PPT_L PPT_R SCP SLF_L SLF_R STT_L STT_R UF_L UF_R VOF_L VOF_R ; do
    fslmeants -i all_FA_skeletonised.nii.gz -m /home/ern24/Bin/DTITK/iit_human_atlas/IIT_bundles_skeletonized_256/${bundle}_skeletonized_256.nii.gz -o meants_FA_${bundle}.txt
done  
