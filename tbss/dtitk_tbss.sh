## Spatially normalize your data into the corresponding group-specific DTI template in 5 steps
# Step 1: Take FSL-generated DTI eigensystem volumes (non-brain tissue already removed) and convert them into fully DTI-TK compatible and preprocessed DTI volumes. 

fsl_to_dtitk dtifit 
TVResample -in dtifit_dtitk.nii.gz -size 128 128 64 
# Store all outputs in one directory
mkdir /data/ern24/DTIprocessed/DTITK
mkdir /data/ern24/DTIprocessed/DTITK/inputvols
cp dtifit_dtitk.nii.gz /data/ern24/DTIprocessed/DTITK/inputvols

# Step 2: Bootstrapping the initial DTI template from the input DTI volumes with the help of existing IXI ageing template (option 1)
cd /data/ern24/DTIprocessed/DTITK/inputvols
ls >> subjs.txt
dti_template_bootstrap /home/ern24/Bin/DTITK/ixi_aging_template_v3.0/template/ixi_aging_template.nii.gz subjs.txt

# Inspect template before proceeding

# SKIP Step 3 (used existing template)

# Step 4: AFFINE ALIGNMENT OF DTI VOLUMES

dti_affine_population template.nii.gz subjs.txt EDS 3

# Step 5: DEFORMABLE ALIGNMENT OF DTI VOLUMES. Outputs regsitered image (two interpolation steps)
TVtool -in template.nii.gz -tr
BinaryThresholdImageFilter template_tr.nii.gz mask.nii.gz 0.01 100 1 0
dti_diffeomorphic_population template.nii.gz subjs_aff.txt mask.nii.gz 0.002


## You can compute the warped FA map of a subject DTI volume in two ways. One is to warp the DTI volume and then compute the FA map. Or you can first compute the FA map then warp the FA map. Many people in the DTI community argue that the former is preferred because the interpolation accounts for the tensorial properties.
# First option:
# Second option:
deformationScalarVolume -in subj_fa.nii.gz -trans subj_combined.df.nii.gz -target mean_initial.nii.gz  -out subj_fa_combined.nii.gz # note this is just for one subject


## Custom implementation of tbss_3_postreg

# To map native subjects to template space using single interpolation operation instead:
dti_warp_to_template_group subjs.txt template.nii.gz 1 1 1

# Generate population-specific DTI template
TVMean -in subjs_normalized.txt -out mean_final_high_res.nii.gz

# Generate FA map of DTI template 
TVtool -in mean_final_high_res.nii.gz -fa
mv mean_final_high_res_fa.nii.gz mean_FA.nii.gz

# Generate WM skeleton
tbss_skeleton -i mean_FA -o mean_FA_skeleton

# Generate FA map of spatially normalised DTI data (i.e. individual subjects)
TVtool -in ${subjs} -out ${subjs}_FA -fa
fslmerge -t all_FA sub*_FA
fslmaths all_FA -max 0 -Tmin -bin mean_FA_mask -odt char

mkdir /data/ern24/DTIprocessed/DTITK/tbss
mkdir /data/ern24/DTIprocessed/DTITK/tbss/stats
cp mean_FA_skeleton all_FA mean_FA_mask /data/ern24/DTIprocessed/DTITK/tbss/stats

## Remainder of tbss pipeline

tbss_4_prestats

stats
