ROI_list <-read.csv("/home/ern24/Bin/BN/tract_seed_masks/seeds.csv", header=TRUE)
ROI_lobe<-subset(ROI_list, select=lobe_region)
names(ROI_lobe)<- NULL

# get file list

subjs_matrices <- dir("/data/ern24/DTIprocessed", recursive=TRUE, full.names=TRUE, pattern="fdt_network_matrix$")

# Make function to plot each matrix

plotMatrix <- function(f){
    subj <- read.csv(f, header=FALSE, sep="")
    mat = as.matrix(subj)
    
    rownames(mat)<-ROI_lobe[,1]
    colnames(mat)<-ROI_lobe[,1]
    
    l<-log(mat+1)
    # set colour limits
  upper = 20
  lower = -1.2
  
  # plot association matrix 
  # define filename
  out_plot = gsub("matrix", "matrix.png", f)

  # set lenght of colour scale
  col.length = 

  # set the number of pracels
  nparcels = 252

  # start the plot
  png(out_plot,
      width = 2400, height = 2400, res = 300)

  # set margins
  par(mar = c(6, 6, 1, 1))

  # draw the association matrix
  image(t(apply(l, 1, rev)),
        col = hcl.colors(col.length, palette = "RdGy", rev = TRUE),
        breaks = c(seq(lower, upper, length.out = col.length+1)), 
        axes = FALSE)

  dev.off()

}

sapply(subjs_matrices, plotMatrix)
