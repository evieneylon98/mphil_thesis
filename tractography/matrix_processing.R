# Process raw connectivity matrices ready for analysis (i.e. by symmetrisation and normalisation)

# No distance correction?
subjs_matrices <- dir("/home/ern24/rds/hpc-work/data/DTIprocessed", recursive=TRUE, full.names=TRUE, pattern="fdt_network_matrix_nopd$")
subjs_waytotals <- dir("/home/ern24/rds/hpc-work/data/DTIprocessed", recursive=TRUE, full.names=TRUE, pattern="waytotal_nopd")

# Distance corrected
#subjs_matrices <- dir("/home/ern24/rds/hpc-work/data/DTIprocessed", recursive=TRUE, full.names=TRUE, pattern="fdt_network_matrix$")
#subjs_waytotals <- dir("/home/ern24/rds/hpc-work/data/DTIprocessed", recursive=TRUE, full.names=TRUE, pattern="waytotal")


# Symmetrise raw matrix

symMatrix <- function(f){
    subj <- read.csv(f, header=FALSE, sep="")
    mat = as.matrix(subj)

    mat_trans <- t(mat)

    mat_sym <- (mat + mat_trans)/2
    
    # Define filename of output matrix
    out_mat = gsub("matrix_nopd", "symmatrix_nopd", f)

    write.table(mat_sym, file=out_mat, sep=" ", col.names=FALSE, row.names=FALSE)

}

sapply(subjs_matrices, symMatrix)


# Symmetrise normalised matrix

normMatrix <- function(x, y){
    subj <- read.csv(x, header=FALSE, sep="")
    mat = as.matrix(subj)
    waytot <- read.csv(y, header=FALSE)
    wt <- as.matrix(waytot)

    wt_plus <- matrix(wt, nrow=252, ncol=252, byrow=FALSE)

    mat_norm <- mat/wt_plus

    mat_norm_trans <- t(mat_norm)

    mat_norm_sym <- (mat_norm + mat_norm_trans)/2

    # Define filename of output matrix
    out_mat = gsub("matrix_nopd", "symmatrix_nopd_norm", f)

    write.table(mat_norm_sym, file=out_mat, sep=" ", col.names=FALSE, row.names=FALSE)

}

mapply(normMatrix, subjs_matrices, subjs_waytotals)

# For subjects with problem seeds

psp_20378_20150707 <- list.files(path="/home/ern24/rds/hpc-work/data/DTIprocessed/sub-20378/ses-20150707/bedpostX/tractography_output_BN252_nopd", full.names=TRUE, pattern="fdt_network_matrix_nopd$")
psp_20378_20150707_wt <- list.files(path="/home/ern24/rds/hpc-work/data/DTIprocessed/sub-20378/ses-20150707/bedpostX/tractography_output_BN252_nopd", full.names=TRUE, pattern="waytotal_nopd")

subj <- read.csv(psp_20378_20150707, header=FALSE, sep="")
mat = as.matrix(subj)
waytot <- read.csv(psp_20378_20150707_wt, header=FALSE)
wt <- as.matrix(waytot)

wt_plus <- matrix(wt, nrow=252, ncol=252, byrow=FALSE)
mat_norm <- mat/wt_plus

ROI = 27
aROI = 15
bROI = 29
cROI = 39

row_av = (mat_norm[aROI,] + mat_norm[bROI,] + mat_norm[cROI,])/3
col_av = (mat_norm[,aROI] + mat_norm[,bROI] + mat_norm[,cROI])/3

mat_norm[ROI,] <- row_av
mat_norm[,ROI] <- col_av
mat_norm[ROI,ROI] = 0

mat_norm_trans <- t(mat_norm)

mat_norm_sym <- (mat_norm + mat_norm_trans)/2

# Define filename of output matrix
out_mat = gsub("matrix_nopd", "symmatrix_nopd_norm_nbs", psp_20378_20150707)
write.table(mat_norm_sym, file=out_mat, sep=" ", col.names=FALSE, row.names=FALSE)



con_16672_20140206 <- list.files(path="/home/ern24/rds/hpc-work/data/DTIprocessed/sub-16672/ses-20140206/bedpostX/tractography_output_BN252_nopd", full.names=TRUE, pattern="fdt_network_matrix_nopd$")
con_16672_20140206_wt <- list.files(path="/home/ern24/rds/hpc-work/data/DTIprocessed/sub-16672/ses-20140206/bedpostX/tractography_output_BN252_nopd", full.names=TRUE, pattern="waytotal_nopd")

subj <- read.csv(con_16672_20140206, header=FALSE, sep="")
mat = as.matrix(subj)
waytot <- read.csv(con_16672_20140206_wt, header=FALSE)
wt <- as.matrix(waytot)

wt_plus <- matrix(wt, nrow=252, ncol=252, byrow=FALSE)
mat_norm <- mat/wt_plus

ROI = 203
aROI = 10
bROI = 200
cROI = 201

row_av = (mat_norm[aROI,] + mat_norm[bROI,] + mat_norm[cROI,])/3
col_av = (mat_norm[,aROI] + mat_norm[,bROI] + mat_norm[,cROI])/3

mat_norm[ROI,] <- row_av
mat_norm[,ROI] <- col_av
mat_norm[ROI,ROI] = 0

mat_norm_trans <- t(mat_norm)

mat_norm_sym <- (mat_norm + mat_norm_trans)/2

# Define filename of output matrix
out_mat = gsub("matrix_nopd", "symmatrix_nopd_norm_nbs", con_16672_20140206)
write.table(mat_norm_sym, file=out_mat, sep=" ", col.names=FALSE, row.names=FALSE)
