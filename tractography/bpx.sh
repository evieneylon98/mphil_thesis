#!/bin/bash

# Prepare bedpost input files

cd bpx_input

mv dwi_biascorr.nii data.nii
mv grad.bval bvals 
mv grad.bvec bvecs

cd ..

#mv data.nii bvals bvecs nodif_brain_mask.nii bedpostx_gpu_input

# Run bedpost

bedpostx bpx_input --model=1



