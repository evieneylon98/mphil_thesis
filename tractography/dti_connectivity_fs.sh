## Prepare seed regions - streamlines are seeded from each voxel in this mask. Can either be a volumetric or a surface file.

cwd=`pwd`

#cd psub*

# Freesurfer linear registration from conformed to diffusion space
# Freesurfer surface (get conformed to diffusion space transform): linear transformation from FS T1w space to DWI native space (FS->T1w->DTI)

# Struct to freesurfer
tkregister2 --mov $cwd/psub*/mri/orig.mgz --targ $cwd/psub*/mri/rawavg.mgz --regheader --reg junk --fslregout freesurfer2struct.mat --noedit
convert_xfm -omat struct2freesurfer.mat -inverse freesurfer2struct.mat

# Diff to struct
flirt -in bpx_input.bedpostX/mean_f1samples.nii -ref preproc/T1w_brain_unbiased.nii -omat f12struct.mat
convert_xfm -omat struct2f1.mat -inverse f12struct.mat

convert_xfm -omat f12freesurfer.mat -concat struct2freesurfer.mat f12struct.mat
convert_xfm -omat freesurfer2f1.mat -inverse f12freesurfer.mat

#mriconvert orig.mgz orig.nii.gz


## Get seeds

# 210 Cortical seeds
cd psub*/label
mkdir BN
cd ..
cd mri
mkdir BN
cd ..
cd ..
mri_annotation2label --subject $cwd/psub* --hemi rh --annotation BN_Atlas --labelbase $cwd/psub*/label/BN/BN-rh
mri_annotation2label --subject $cwd/psub* --hemi lh --annotation BN_Atlas --labelbase $cwd/psub*/label/BN/BN-lh

for labelrh in `ls $cwd/psub*/label/BN/BN-rh*`; do
    volname="${labelrh%.label}"
    mri_label2vol --subject $cwd/psub* --hemi rh --label $cwd/psub*/label/BN/$labelrh --temp $cwd/psub*/mri/orig.mgz --o $cwd/psub*/mri/BN/${volname}.nii --proj abs -1 0 0.1 --identity ;
done

for labellh in `ls $cwd/psub*/label/BN/BN-lh*`; do
    volname="${labellh%.label}"
    mri_label2vol --subject $cwd/psub* --hemi lh --label $cwd/psub*/label/BN/$labellh --temp $cwd/psub*/mri/orig.mgz --o $cwd/psub*/mri/BN/${volname}.nii --proj abs -1 0 0.1 --identity ;
done

# 36 Subcortical seeds

mri_convert $cwd/psub*/mri/BN_Atlas_subcotex_clean_and_cortex_and_aseg_cerebDC_and_brain-stem.mgz $cwd/psub*/mri/BN_Atlas_subcotex_clean_and_cortex_and_aseg_cerebDC_and_brain-stem.nii.gz 

for roinum in 211 212 213 214 215 216 217 218 219 220 221 222 223 224 225 226 227 228 229 230 231 232 233 234 235 236 237 238 239 240 241 242 243 244 245 246 ; do
    fslmaths $cwd/psub*/mri/BN_Atlas_subcotex_clean_and_cortex_and_aseg_cerebDC_and_brain-stem.nii.gz -thr $roinum -uthr $roinum BN-${roinum} ;
done 

# Additional freesurfer seeds


## Run probabilistic tracking from bedpost directory in subject's native space:

probtrackx2 --samples merged \
            --mask nodif_brain_mask.nii \
            --seed NIFTI/surface \
            --out probtrackx2 \
            --avoid  \
            --stop  \
            --wtstop  \
            --omatrix1  \
            --omatrix3  \
            --xfm seed2diff (if seed not in diffusion space)\
            --invxfm diff2seed \
            --seedref=orig.nii.gz (ref image if seed is a surface) \
            --meshspace=freesurfer \
            --nsamples (no of streamlines drawn from each voxel/surface vertex) \
            --nsteps [2000] \
            --steplength [0.5] adjust according to voxel size being used \
            --distthresh 10  \
            --cthr [0.2]  \
            --usef 0.7 \
            --loopcheck \
            --network ? requires ASCII file listing ROI seed masks to be provided \
            --pd \
            --opd \
