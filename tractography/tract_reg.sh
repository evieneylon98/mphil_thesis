#!/bin/bash

fslpath=/applications/fsl/fsl-6.0.1/install/data/standard
bnpath=/home/ern24/Bin/BN/brainnetome-for-freesurfer/mni_icbm152_t1_tal_nlin_asym_09c_brain/mri

#mni_icbm152_t1_tal_nlin_asym_09c_brain.nii.gz

# In main subject directory...


## Get diff2standard transformations and inverses


# First go from diffusion to structural space
# 6 DOF, CC, normal search; affine FLIRT
# epi_reg b0/mean_f1samples to T1

N4BiasFieldCorrection -d 3 -i *T1w.nii* -o T1w_unbiased.nii -s 4 -b [100,3] -v 1

flirt -in bpx_input.bedpostX/mean_f1samples.nii -ref preproc/T1w_brain_unbiased.nii -omat diff2str.mat -out diff2straff.nii -dof 12

convert_xfm -omat str2diff.mat -inverse diff2str.mat



# Next go from structural to standard MNI152 space
# 12 DOF, CC, normal search
# FLIRT and FNIRT to 2mm MNI
# FNIRT requires non-brain extracted structural image.


flirt -in preproc/T1w_brain_unbiased.nii -ref ${fslpath}/MNI152_T1_2mm_brain.nii.gz -dof 12 -omat str2standard_flirt.mat

##fnirt --in=T1w_unbiased.nii --ref=${fslpath}/MNI152_T1_2mm.nii.gz --aff=str2standard_flirt.mat --refmask=${fslpath}/MNI152_T1_2mm_brain_mask_dil.nii.gz --cout=str2standard 

fnirt --in=T1w_unbiased.nii --aff=str2standard_flirt.mat --cout=str2standard --config=T1_2_MNI152_2mm

invwarp --ref=T1w_unbiased.nii --warp=str2standard.nii --out=standard2str.nii

# Get T1 brain in MNI space (high resolution to aid segmentation)

#applywarp -i preproc/T1w_brain_unbiased.nii -r ${fslpath}/MNI152_T1_1mm_brain.nii.gz -o T1w_braininMNI.nii -w str2standard.nii


# Concatenate

convertwarp --ref=${fslpath}/MNI152_T1_2mm_brain.nii.gz --out=diff2standard.nii --warp1=str2standard.nii --premat=diff2str.mat

invwarp --ref=bpx_input.bedpostX/mean_f1samples.nii --warp=diff2standard.nii --out=standard2diff.nii

#mv diff2str.mat diff2standard.nii str2diff.mat str2standard.nii standard2str.nii standard2diff.nii bpx_input.bedpostX/xfms



## Repeat for BN MNI template

# Structural to MNI152

flirt -in preproc/T1w_brain_unbiased.nii -ref ${bnpath}/mni_icbm152_t1_tal_nlin_asym_09c_brain.nii.gz -dof 12 -omat str2BNstandard_flirt.mat

fnirt --in=T1w_unbiased.nii --aff=str2BNstandard_flirt.mat --cout=str2BNstandard --config=${bnpath}/mni_icbm152_t1_tal_nlin_asym_09c.cnf

invwarp --ref=T1w_unbiased.nii --warp=str2BNstandard.nii --out=BNstandard2str.nii


# Concatenate

convertwarp --ref=${bnpath}/mni_icbm152_t1_tal_nlin_asym_09c_brain.nii.gz --out=diff2BNstandard.nii --warp1=str2BNstandard.nii --premat=diff2str.mat

invwarp --ref=bpx_input.bedpostX/mean_f1samples.nii --warp=diff2BNstandard.nii --out=BNstandard2diff.nii


mv diff2str.mat diff2standard.nii str2diff.mat str2standard.nii standard2str.nii standard2diff.nii str2BNstandard.nii BNstandard2str.nii diff2BNstandard.nii BNstandard2diff.nii bpx_input.bedpostX/xfms



