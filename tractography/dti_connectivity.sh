## Prepare seed region - streamlines are seeded from each voxel in this mask. Can either be a volumetric or a surface file.
## All masks must be provided in the same space.

# tract_reg already run

# GM/WM boundary mask (from T1 in MNI space)

mrconvert T1w_braininMNI.nii T1w_braininMNI.mif

5ttgen fsl T1w_braininMNI.mif 5tt_T1w_braininMNI.mif -premasked -debug

#fast T1w_brain.nii -t 1 -o T1w_FAST -S 1 -n 3

5tt2gmwmi 5tt_T1w_braininMNI.mif gmwmBoundary.mif -debug

mrconvert gmwmBoundary.mif gmwmBoundary.nii


## Termination masks to stop the streamlines. Note that paths are always terminated when they reach the brain surface as defined by nodif_brain_mask

grey matter?


## Exclusion masks? (e.g. of midline to remove pathways that cross into the other hemisphere)??


## Run probabilistic tracking/streamline generation from bedpost directory in subject's native space:

probtrackx2 --samples=merged \
            --mask=nodif_brain_mask.nii \
            --seed=gmwmBoundary.nii \
            --out=probtrackx \
            --avoid=csf_mask.nii \
            --stop=gm_mask.nii \
            --omatrix1 \
            --xfm=xfms/standard2diff.nii \
            --invxfm=xfms/diff2standard.nii \
            --seedref=/applications/fsl/fsl-6.0.1/install/data/standard/MNI152_T1_2mm_brain.nii.gz \
            --nsamples=5000 \
            --nsteps=2000 \
            --steplength=0.5 \
            --distthresh=10 \
            --cthr=0.2 \
            --usef=0.15 \
            --loopcheck \
            --pd \
            --opd \
            --ompl \
            --sampvox=0.5

            change stop to wtstop using gmwmBoundary?
            output is in 1mm resolution bcos seed is in 1mm resolution. change to 2mm for more chance of streamlines passing through each voxel
            remove sampvox


## Parcellate brain into different regions using Brainnetome atlas.

## Create connectome
