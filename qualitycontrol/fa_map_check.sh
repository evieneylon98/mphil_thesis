#!/bin/bash

## Batches FA maps into one folder and creates html page to allow for identification of major problems (e.g. severe distortions, stripy lines) and any issues of head coverage.
## Run from /group/p00259/dti_data
## Remember to create /QC/FA directory

for d in `ls -d DTI/*/*`; do 
  

subj=$1
ses=$2
WORKDIR=$3  

cd DTI/

slicesdir dtifit_fa.nii
cp slicesdir/dtifit_fa.png /group/p00259/dti_data/QC/FA/${subj}_${ses}_dtifit_fa.nii
rm -r slicesdir


cd DTI

for file in */*/preproc/dtifit_fa.nii; do
        l1="${file%%/*}"
        l2="${file#*/}"
        l2="${l2%%/*}"
        filename="${file##*/}"
        target_file_name="${l1}_${l2}_${filename}"
        echo cp "$file" "/group/p00259/dti_data/QC/FA/${target_file_name}"
done

cd QC/FA
slicesdir `imglob *`



cp dtifit_fa.nii /group/p00259/dti_data/QC/FA/

