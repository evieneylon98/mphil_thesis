#!/bin/bash

## Motion metrics:
# 1. Extract average absolute and relative motion and % of outliers from json files.

awk -F' ' '{ total += $1 } END {print total/NR}' dwi_post_eddy.eddy_movement_rms > motion_metrics.txt

awk -F' ' '{ total += $2 } END {print total/NR}' dwi_post_eddy.eddy_movement_rms >> motion_metrics.txt

jq .qc_outliers_tot *json >> motion_metrics.txt 

# 2. Get total movement throughout acquisition (Rae et al)
#i.e. sum 2nd column in eddy_movement_rms

awk -F' ' '{ total += $2 } END {print total}' dwi_post_eddy.eddy_movement_rms >> motion_metrics.txt

# switch from one column to one row

awk ' BEGIN { ORS=" " } { print } ' motion_metrics.txt > motion_metrics1.txt
rm motion_metrics.txt
mv motion_metrics1.txt motion_metrics.txt

# create one file for all subjects

gvim

# combine all motion metrics

# add column



##QC:

#Outlier slices
# consider removing if >10%

# Relative movement between volumes
# subtract mean from each value; is resulting value within 2SD?
