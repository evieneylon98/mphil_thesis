#!/bin/bash 

# This script allows you to cycle through your subject directories automatically and submit jobs to the slurm scheduler.
# {DTI/sub-*/ses-*} should be set to your subject directories 
# Alter path to your slurm file

cwd=`pwd`
for i in `ls -d DTI/sub-*/ses-*`; do
 echo $i
 cd $i
 sbatch /group/p00259/dti_data/slurm_preproc.hphi
 cd $cwd
done
