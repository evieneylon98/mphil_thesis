#!/bin/bash

## Optional script for EPI distortion correction if no topup/fieldmaps

# To be performed directly after eddy correction 
# Requires skull-stripped T1-weighted image.

cp T1w_brain.nii preproc

cd preproc

mkdir EPI_dist_corr

cp T1w_brain.nii rawdata_corrected.mif b0_processing.nii.gz EPI_dist_corr

cd EPI_dist_corr



# Prepare input images

N4BiasFieldCorrection -d 3 -i T1w_brain.nii -o T1w_brain_unbiased.nii -s 4 -b [100,3] -v 1

mrgrid T1w_brain_unbiased.nii pad -uniform 10 T1w_brain_padded.nii


fslmaths T1w_brain_padded.nii -thr 0 -bin T1_nonzero.nii
T1_minmax=`fslstats T1w_brain_padded.nii -k T1_nonzero.nii -R`
T1_min=`echo "$T1_minmax" | cut -d ' ' -f 1`
T1_max=`echo "$T1_minmax" | cut -d ' ' -f 2`

echo $T1_min

fslmaths b0_processing.nii.gz -thr 0 -bin b0_nonzero.nii
b0_minmax=`fslstats b0_processing.nii.gz -k b0_nonzero.nii -R`
b0_min=`echo "$b0_minmax" | cut -d ' ' -f 1`
b0_max=`echo "$b0_minmax" | cut -d ' ' -f 2`

echo $b0_min

T1_range=$(echo "$T1_max - $T1_min" | bc)
b0_range=$(echo "$b0_max - $b0_min" | bc)


fslmaths T1w_brain_padded.nii -mul -1 -add $T1_max -mul $T1_range -div $b0_range -mul T1_nonzero.nii T1w_brain_inv.nii



# Upsample moving image to voxel size of target image to retain high resolution structural scan

mrgrid b0_processing.nii.gz regrid -voxel 1.25 b0_processing_highres.nii.gz



# Rigid registration: b0 to T1

antsRegistration -d 3 \
		 --float 0 \
		 -o struct_target \
		 -u 0 \
		 -t Rigid[0.15] \
	 	 -m MI[T1w_brain_inv.nii,b0_processing.nii.gz,1,32,Regular,0.25] \
		 -c [1000x500x250x100,1e-6,10] \
		 -f 8x4x2x1 \
		 -s 3x2x1x0 \
		 -v 1

# Apply inverse to T1 to place T1 in DWI space. Generate 2 targets at different resolutions

antsApplyTransforms -d 3 \
		    -n Linear \
		    -i T1w_brain_inv.nii \
		    -o struct_target_highres.nii \
		    -r b0_processing_highres.nii.gz \
		    -t [struct_target0GenericAffine.mat, 1] \
		    -v 1

antsApplyTransforms -d 3 \
		    -n Linear \
		    -i T1w_brain_inv.nii \
		    -o struct_target.nii \
		    -r b0_processing.nii.gz \
		    -t [struct_target0GenericAffine.mat, 1] \
		    -v 1



# Non-linearly register B0 to T1 (at high res- more accurate registration)

antsRegistration -d 3 \
		 --float 0 \
		 -o b02target_SyN \
		 -u 0 \
	 	 -t BSplineSyN[0.1,30,0,3] \
		 -m MI[struct_target_highres.nii,b0_processing.nii.gz,1,32,Regular,0.25] \
		 -g 0.2x1x0.2 \
		 -c [100x70x50x20,1e-6,10] \
		 -s 2x1x0x0 \
		 -f 4x2x2x1 \
		 -v 1

# Apply high res transform using downsampled structural target to prevent oversampling of DWI data

#antsApplyTransforms -d 3 \
		    -n BSpline \
		    -i b0_processing.nii.gz \
		    -o b0_undistorted.nii \
		    -r struct_target.nii \
		    -t b02target_SyN0Warp.nii.gz \
		    -v 1

mrconvert rawdata_corrected.mif rawdata_corrected.nii -force

antsApplyTransforms -d 3 \
		    -e 3 \
		    -n BSpline \
		    -i rawdata_corrected.nii \
		    -o rawdata_undistorted.nii \
		    -r struct_target.nii \
		    -t b02target_SyN0Warp.nii.gz \
		    -v 1


CreateJacobianDeterminantImage 3 b02target_SyN0Warp.nii.gz jacob_determ.nii.gz 0 1

CreateJacobianDeterminantImage 3 b02target_SyN0Warp.nii.gz jacob_determ_log.nii.gz 1 1

fslorient -copyqform2sform rawdata_undistorted_redo.nii


cp rawdata_undistorted.nii T1w_brain_unbiased.nii ..

cd ..




# Get B0 mask for bias correction/other processing

mrconvert rawdata_undistorted.nii b0_undistorted.nii -force -nthreads 1 -coord 3 0

bet2 b0_undistorted.nii b0_undistorted_brain -m -f 0.10 


#Bias field correction to improve mask estimation (and check)

dwibiascorrect ants rawdata_undistorted.nii dwi_biascorr.nii -force -nthreads 1 -mask b0_undistorted_brain_mask.nii -fslgrad grad.bvec grad.bval


#generate mask (and check)

dwi2mask dwi_biascorr.nii dwi2mask.nii -force -nthreads 1 -fslgrad grad.bvec grad.bval
 


#if holes in this mask then extract unbiased b0 mask

dwiextract dwi_biascorr.nii -fslgrad grad.bvec grad.bval - -bzero | mrmath - mean nodif.nii -axis 3 -force -nthreads 1

bet nodif.nii nodif_brain -R -m -f 0.15

#gunzip nodif_brain_mask.nii.gz -f



echo "Done"



