#!/bin/sh

## In main subject directory. Must adjust to work with your file names.

mkdir -p preproc

cp *dwi*.nii.gz rawdata.nii.gz
cp *dwi*.bval rawdata.bval
cp *dwi*.bvec rawdata.bvec
#cp *dwi*.json rawdata.json

cp rawdata.nii.gz rawdata.bval rawdata.bvec rawdata.json slspec.txt preproc

cd preproc


# Need to convert files from nii to mif (MRtrix) format before and embed gradient tables

mrconvert rawdata.nii.gz rawdata.mif -fslgrad rawdata.bvec rawdata.bval -strides -1,2,3,4 -force -nthreads 1


# Denoise

dwidenoise rawdata.mif rawdata_dn.mif -force -nthreads 1

#mrcalc rawdata.mif rawdata_dn.mif -subtract res.mif -force


# Remove Gibbs' ringing artefacts (inspect data first using mrview to see if necessary; not designed to be used on patial fourier data)

#mrdegibbs rawdata_dn.mif outputfile.mif


# Eddy correction, motion correction & susceptibility-induced distortion correction. Might need to make mporder a conditional value. 
# Note dwifslpreprocern24 is an edited version of dwifslpreproc that forces use of eddy_cuda8.0 (see fsl eddy userguide)
# Adjust options to suit your data.

dwifslpreprocern24 rawdata_dn.mif rawdata_corrected.mif -rpe_none -pe_dir j- -export_grad_fsl grad.bvec grad.bval -debug -force -readout_time 0.0342002 -json_import rawdata.json -nocleanup -nthreads 1 -eddy_options " --mporder=16 --repol" -eddy_slspec slspec.txt


## Brain mask estimation


# Get B0 mask for bias correction/other processing

mrconvert rawdata_corrected.mif rawdata_b0.nii -force -nthreads 1 -coord 3 0

bet2 rawdata_b0.nii b0_processing -m -f 0.10


# Bias field correction to improve mask estimation (and check)

dwibiascorrect ants rawdata_corrected.mif dwi_unbiased.mif -force -nthreads 1 -mask b0_processing_mask.nii.gz


#generate mask (and check)

#dwi2mask rawdata_unbiased.mif rawdata_mask.nii -force -nthreads 1

#mrview rawdata_mask.nii 


# If holes in this mask then extract unbiased b0 mask

dwiextract dwi_unbiased.mif - -bzero | mrmath - mean nodif.nii -axis 3 -force -nthreads 1

bet2 nodif.nii nodif_brain -m -f 0.15

#gunzip nodif_brain_mask.nii.gz -f




echo "Done"

