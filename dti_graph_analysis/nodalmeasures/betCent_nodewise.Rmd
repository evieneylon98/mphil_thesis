---
title: "Betweenness Centrality"
output: html_document
---

```{r}
library(dplyr)
library(dbplyr)
library(RSQLite)
library(DT)
library(ggplot2)
library(xtable)
library(rstan)
library(coda)
library(tidyr)
library(broom)
library(broom.mixed)
library(multcomp)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)
set.seed(1)

graph.db <- DBI::dbConnect(RSQLite::SQLite(), "/home/ern24/rds/hpc-work/data/graphResults/graphResults.db")

diagnosis.order = c("control",
                    "psp-sub",
                    "psp-RS",
                    "psp-cort")
```


```{r}

metric = "betCentNorm"
metric_z = paste(metric, "z", sep = "_")
metric.name = "Betweenness Centrality (Normalised)"

graph.data = dbGetQuery(graph.db, sql(paste("SELECT DISTINCT ", metric, ".", metric, ", ",
                                            metric, ".node, ", metric, ".edgePC, ",
                                            metric, ".session_id, sessionData.subj, ",
                                            "sessionData.session, diagnosis.diag_group",
                                     " FROM sessionData",
                                     " LEFT JOIN ", metric, ", diagnosis ON sessionData.session_id = ", metric, ".session_id",
                                                                   " AND sessionData.subj = diagnosis.subj",
                                     " WHERE (diagnosis.diag_group = 'control' ",
                                     "OR diagnosis.diag_group = 'psp-cort' ",
                                     "OR diagnosis.diag_group = 'psp-sub' ",
                                     "OR diagnosis.diag_group = 'psp-RS') ",
                                    sep = "")))


```
```{r}
# Reduce data to chosen threshold

graph.data <- subset(graph.data, edgePC == 8)
```

```{r}

# Test each node

for (n in 1:252) {
    nodal_subset <- subset(graph.data, node == n)
    fit <- aov(betCentNorm~diag_group, nodal_subset)
    aovresult <- summary(fit)
    result <- aovresult[[1]]$'Pr(>F)'

    # Create the first data if no data exist yet
  if (!exists("pvalues")){
    pvalues <- c(n , result)
    pvalues <- data.frame(pvalues)
    pvalues <- as.data.frame(t(pvalues))
  }
  
  # if data already exist, then append it together
  if (exists("pvalues")){
    pvalues <- unique(rbind(pvalues, c(n , result)))
  }

}

pvalues <- pvalues[-3]
colnames(pvalues)[1] <- "node_no"
colnames(pvalues)[2] <- "uncorrected_pvalue"

corrected_pvalues <- p.adjust(pvalues[,2], method = "BY", n = 252)

pvalues <- cbind(pvalues, corrected_pvalues)
colnames(pvalues)[3] <- "corrected_pvalue"

# Add node labels

node_labels <- read.csv("/home/ern24/Bin/BN/tract_seed_masks/seeds.csv", header=T)
node_labels <- subset(node_labels, select = c(cyto, gyrus, lobe_region))
pvalues <- cbind(node_labels, pvalues)

pvalues_sig <- subset(pvalues, corrected_pvalue < 0.01)

print(pvalues_sig)

```

Repeat without motion outliers
```{r}
graph.data_nooutliers <- graph.data[!(graph.data$session_id==129 |
                                      graph.data$session_id==120 |
                                      graph.data$session_id==91 |
                                        graph.data$session_id==89 |
                                        graph.data$session_id==90 |
                                        graph.data$session_id==104 |
                                        graph.data$session_id==17 |
                                        graph.data$session_id==110 |
                                        graph.data$session_id==73 |
                                        graph.data$session_id==79 |
                                        graph.data$session_id==53 |
                                        graph.data$session_id==9 |
                                        graph.data$session_id==137 |
                                        graph.data$session_id==116 |
                                        graph.data$session_id==119 |
                                        graph.data$session_id==24 |
                                        graph.data$session_id==145),]

# Test each node

for (n in 1:252) {
    nodal_subset <- subset(graph.data_nooutliers, node == n)
    fit <- aov(betCentNorm~diag_group, nodal_subset)
    aovresult <- summary(fit)
    result <- aovresult[[1]]$'Pr(>F)'

    # Create the first data if no data exist yet
  if (!exists("pvalues_nooutliers")){
    pvalues_nooutliers <- c(n , result)
    pvalues_nooutliers <- data.frame(pvalues_nooutliers)
    pvalues_nooutliers <- as.data.frame(t(pvalues_nooutliers))
  }
  
  # if data already exist, then append it together
  if (exists("pvalues_nooutliers")){
    pvalues_nooutliers <- unique(rbind(pvalues_nooutliers, c(n , result)))
  }

}

pvalues_nooutliers <- pvalues_nooutliers[-3]
colnames(pvalues_nooutliers)[1] <- "node_no"
colnames(pvalues_nooutliers)[2] <- "uncorrected_pvalue"

corrected_pvalues_nooutliers <- p.adjust(pvalues_nooutliers[,2], method = "BY", n = 252)

pvalues_nooutliers <- cbind(pvalues_nooutliers, corrected_pvalues_nooutliers)
colnames(pvalues_nooutliers)[3] <- "corrected_pvalue"

# Add node labels

node_labels <- read.csv("/home/ern24/Bin/BN/tract_seed_masks/seeds.csv", header=T)
node_labels <- subset(node_labels, select = c(cyto, gyrus, lobe_region))
pvalues_nooutliers <- cbind(node_labels, pvalues_nooutliers)

pvalues_nooutliers_sig <- subset(pvalues_nooutliers, corrected_pvalue < 0.01)

print(pvalues_nooutliers_sig)
```

