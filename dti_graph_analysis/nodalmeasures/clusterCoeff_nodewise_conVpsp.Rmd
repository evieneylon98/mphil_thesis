---
title: "Clustering Coefficient"
output: html_document
---

```{r}
library(dplyr)
library(dbplyr)
library(RSQLite)
library(DT)
library(ggplot2)
library(xtable)
library(rstan)
library(coda)
library(tidyr)
library(broom)
library(broom.mixed)
library(multcomp)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)
set.seed(1)

graph.db <- DBI::dbConnect(RSQLite::SQLite(), "/home/ern24/rds/hpc-work/data/graphResults/graphResults.db")

diagnosis.order = c("control",
                    "psp")
```


```{r}

metric = "ccNorm"
metric_z = paste(metric, "z", sep = "_")
metric.name = "Clustering Coefficient (Normalised)"

graph.data = dbGetQuery(graph.db, sql(paste("SELECT DISTINCT ", metric, ".", metric, ", ",
                                            metric, ".node, ", metric, ".edgePC, ",
                                            metric, ".session_id, sessionData.subj, ",
                                            "sessionData.session, diagnosis.diag",
                                     " FROM sessionData",
                                     " LEFT JOIN ", metric, ", diagnosis ON sessionData.session_id = ", metric, ".session_id",
                                                                   " AND sessionData.subj = diagnosis.subj",
                                     " WHERE (diagnosis.diag = 'control' ",
                                     "OR diagnosis.diag = 'psp') ",
                                    sep = "")))


```
```{r}
# Reduce data to chosen threshold

graph.data <- subset(graph.data, edgePC == 8)
```

```{r}

# Test each node

for (n in 1:252) {
    nodal_subset <- subset(graph.data, node == n)
    nodal_subset <- subset(graph.data, node == n)
    tresult<- t.test(ccNorm ~ diag, data=nodal_subset)
    result <- tresult$p.value

    # Create the first data if no data exist yet
  if (!exists("pvalues")){
    pvalues <- c(n , result)
    pvalues <- data.frame(pvalues)
    pvalues <- as.data.frame(t(pvalues))
  }
  
  # if data already exist, then append it together
  if (exists("pvalues")){
    pvalues <- unique(rbind(pvalues, c(n , result)))
  }

}

pvalues <- pvalues[-3]
colnames(pvalues)[1] <- "node_no"
colnames(pvalues)[2] <- "uncorrected_pvalue"

corrected_pvalues <- p.adjust(pvalues[,2], method = "BY", n = 252)

pvalues <- cbind(pvalues, corrected_pvalues)
colnames(pvalues)[3] <- "corrected_pvalue"

# Add node labels

node_labels <- read.csv("/home/ern24/Bin/BN/tract_seed_masks/seeds.csv", header=T)
node_labels <- subset(node_labels, select = c(cyto, gyrus, lobe_region))
pvalues <- cbind(node_labels, pvalues)

pvalues_sig <- subset(pvalues, corrected_pvalue < 0.01)

print(pvalues_sig)

```

Repeat without motion outliers
```{r}

graph.data_nooutliers <- graph.data[!(graph.data$subj=="sub-13672" |
                                      graph.data$subj=="sub-16257" |
                                      graph.data$subj=="sub-16486" |
                                        graph.data$subj=="sub-18735" |
                                        graph.data$subj=="sub-18829" |
                                        graph.data$subj=="sub-19411" & graph.data$session=="ses-20110406" |
                                        graph.data$subj=="sub-22076" & graph.data$session=="ses-20141001" |
                                        graph.data$subj=="sub-23419" |
                                        graph.data$subj=="sub-23977" |
                                        graph.data$subj=="sub-24033" |
                                        graph.data$subj=="sub-24600" |
                                        graph.data$subj=="sub-18281" |
					graph.data$subj=="sub-22720" & graph.data$session=="ses-20150427" |
					                              graph.data$subj=="sub-23715" |
					graph.data$subj=="sub-24395" & graph.data$session=="ses-20150112" |
                                        graph.data$subj=="sub-21490"),]

            

# Test each node

# for (n in 1:252) {
#     nodal_subset <- subset(graph.data_nooutliers, node == n)
#     tresult<- t.test(ccNorm ~ diag, data=nodal_subset)
#     result <- tresult$p.value
# 
#     # Create the first data if no data exist yet
#   if (!exists("pvalues_nooutliers")){
#     pvalues_nooutliers <- c(n , result)
#     pvalues_nooutliers <- data.frame(pvalues_nooutliers)
#     pvalues_nooutliers <- as.data.frame(t(pvalues_nooutliers))
#   }
#   
#   # if data already exist, then append it together
#   if (exists("pvalues_nooutliers")){
#     pvalues_nooutliers <- unique(rbind(pvalues_nooutliers, c(n , result)))
#   }
# 
# }

library(tidyverse)
library(rstatix)
library(car)

for (n in 1:252) {
    nodal_subset <- subset(graph.data_nooutliers, node == n)
    # Add in covariates
    nuisance <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/nuisance_variables_nooutliers.csv")
    nuisance<- subset(nuisance, select=-c(diag_group))
    colnames(nuisance)[1]<-"subj"
    colnames(nuisance)[2]<-"session"
    nodal_subset <- merge(nodal_subset, nuisance)
    options(contrasts=c("contr.sum", "contr.poly"))
    res.aov<-aov(ccNorm ~ tot_motion + ageAtScan + scanner_type + diag, nodal_subset)
    ancova <- Anova(res.aov, type="III")
    result <- ancova$`Pr(>F)`[5]
    
    # Create the first data if no data exist yet
  if (!exists("pvalues_nooutliers")){
    pvalues_nooutliers <- c(n , result)
    pvalues_nooutliers <- data.frame(pvalues_nooutliers)
    pvalues_nooutliers <- as.data.frame(t(pvalues_nooutliers))
  }

  # if data already exist, then append it together
  if (exists("pvalues_nooutliers")){
    pvalues_nooutliers <- unique(rbind(pvalues_nooutliers, c(n , result)))
  }

}

#pvalues_nooutliers <- pvalues_nooutliers[-3]
colnames(pvalues_nooutliers)[1] <- "node_no"
colnames(pvalues_nooutliers)[2] <- "uncorrected_pvalue"

corrected_pvalues_nooutliers <- p.adjust(pvalues_nooutliers[,2], method = "BY", n = 252)

pvalues_nooutliers <- cbind(pvalues_nooutliers, corrected_pvalues_nooutliers)
colnames(pvalues_nooutliers)[3] <- "corrected_pvalue"

# Add node labels

node_labels <- read.csv("/home/ern24/Bin/BN/tract_seed_masks/seeds.csv", header=T)
node_labels <- subset(node_labels, select = c(cyto, gyrus, lobe_region))
pvalues_nooutliers <- cbind(node_labels, pvalues_nooutliers)

pvalues_nooutliers_sig <- subset(pvalues_nooutliers, corrected_pvalue < 0.05)

print(pvalues_nooutliers_sig)
```

