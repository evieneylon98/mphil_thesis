---
title: "Untitled"
output: html_document
---
```{r}
library(dplyr)
library(dbplyr)
library(RSQLite)
library(DT)
library(ggplot2)
library(xtable)
library(rstan)
library(coda)
library(tidyr)
library(broom)
library(broom.mixed)
library(multcomp)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)
set.seed(1)

graph.db <- DBI::dbConnect(RSQLite::SQLite(), "/home/ern24/rds/hpc-work/data/graphResults/graphResults.db")

diagnosis.order = c("control",
                    "psp")
```


```{r}
metric = "degree_wt"
metric_z = paste(metric, "z", sep = "_")
metric.name = "Weighted degree"

graph.data = dbGetQuery(graph.db, sql(paste("SELECT DISTINCT ", metric, ".", metric, ", ",
                                            metric, ".node, ",
                                            metric, ".session_id, sessionData.subj, ",
                                            "sessionData.session, diagnosis.diag",
                                     " FROM sessionData",
                                     " LEFT JOIN ", metric, ", diagnosis ON sessionData.session_id = ", metric, ".session_id",
                                                                   " AND sessionData.subj = diagnosis.subj",
                                     " WHERE (diagnosis.diag = 'control' ",
                                     "OR diagnosis.diag = 'psp') ",
                                    sep = "")))

graph.data <- graph.data[!(graph.data$subj=="sub-13672" |
                                      graph.data$subj=="sub-16257" |
                                      graph.data$subj=="sub-16486" |
                                        graph.data$subj=="sub-18735" |
                                        graph.data$subj=="sub-18829" |
                                        graph.data$subj=="sub-19411" & graph.data$session=="ses-20110406" |
                                        graph.data$subj=="sub-22076" & graph.data$session=="ses-20141001" |
                                        graph.data$subj=="sub-23419" |
                                        graph.data$subj=="sub-23977" |
                                        graph.data$subj=="sub-24033" |
                                        graph.data$subj=="sub-24600" |
                                        graph.data$subj=="sub-18281" |
					graph.data$subj=="sub-22720" & graph.data$session=="ses-20150427" |
					                              graph.data$subj=="sub-23715" |
					graph.data$subj=="sub-24395" & graph.data$session=="ses-20150112" |
                                        graph.data$subj=="sub-21490"),]

         

# Mean by session_id
dg <- subset(graph.data, select = c(session_id, diag, subj, session))
dg <- unique(dg) 
graph.data <- ddply(graph.data, c("session_id"), summarise, N=252, mean_degree=mean(degree_wt))
graph.data <- merge(graph.data, dg)
metric = "mean_degree"


```

```{r}
p.degree <- ggplot(graph.data, aes_string(x = "diag", y = metric, fill = "diag")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.degree)

```

```{r}

# Add in covariates

library(tidyverse)
library(rstatix)
library(car)

nuisance <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/nuisance_variables_nooutliers.csv")
nuisance<- subset(nuisance, select=-c(diag_group))
colnames(nuisance)[1]<-"subj"

graph.data<- merge(graph.data, nuisance)

# Perform ANCOVA

options(contrasts=c("contr.sum", "contr.poly"))
res.aov<-aov(mean_degree ~ tot_motion + ageAtScan + scanner_type+ diag, graph.data)

result <- Anova(res.aov, type="III")
print(result)
```