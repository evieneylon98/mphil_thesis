---
title: "Local Effiency (Norm)"
output: html_notebook
---

```{r}
library(dplyr)
library(dbplyr)
library(RSQLite)
library(DT)
library(ggplot2)
library(xtable)
library(rstan)
library(coda)
library(tidyr)
library(broom)
library(multcomp)
library(ggpubr)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)
set.seed(1)

```

```{r}
graph.db <- DBI::dbConnect(RSQLite::SQLite(), "/home/ern24/rds/hpc-work/data/graphResults/graphResults.db")

diagnosis.order = c("control",
                    "psp-sub",
                    "psp-RS",
                    "psp-cort")
```

```{r}
metric = "leNorm"
metric_z = paste(metric, "z", sep = "_")
metric.name = "Local Effiency"

graph.data = dbGetQuery(graph.db, sql(paste("SELECT DISTINCT ", metric, ".", metric, ", ",
                                            metric, ".edgePC, ",
                                            metric, ".session_id, sessionData.subj, ",
                                            "sessionData.session, diagnosis.diag_group",
                                     " FROM sessionData",
                                     " LEFT JOIN ", metric, ", diagnosis ON sessionData.session_id = ", metric, ".session_id",
                                                                   " AND sessionData.subj = diagnosis.subj",
                                     " WHERE (diagnosis.diag_group = 'control' ",
                                     "OR diagnosis.diag_group = 'psp-cort' ",
                                     "OR diagnosis.diag_group = 'psp-sub' ",
                                     "OR diagnosis.diag_group = 'psp-RS') ",
                                    sep = "")))

#graph.data$diag_group <- sapply(graph.data$diag_group, function(x) if (grepl("CBS", x)) {return("CBS")} else {return(x)})

graph.data <- graph.data %>%
  mutate(!!metric := as.numeric(.data[[metric]]),
         edgePC = as.numeric(edgePC),
         diag_group = factor(diag_group, levels = diagnosis.order)) %>%
  filter(is.finite(.data[[!!metric]]))

```

```{r}
# plot by edgePC

p.edgePC <- ggplot(graph.data, aes_string(x = "edgePC", y = metric, group = "edgePC")) +
  geom_boxplot() +
  scale_x_continuous(breaks = seq(10), labels = seq(10))
  labs(y = metric.name, x = "Edge threshold (%)")

print(p.edgePC)

```
# Individual thresholds

```{r}
# set threshold
edgePC = 1
edgePC_1 = edgePC
```

## Threshold: `r edgePC_1`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))

```

```{r}
# set threshold
edgePC = 2
edgePC_2 = edgePC
```

## Threshold: `r edgePC_2`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```

```{r}
# set threshold
edgePC = 3
edgePC_3 = edgePC
```

## Threshold: `r edgePC_3`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```


```{r}
# set threshold
edgePC = 4
edgePC_4 = edgePC
```

## Threshold: `r edgePC_4`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```

```{r}
# set threshold
edgePC = 5
edgePC_5 = edgePC
```

## Threshold: `r edgePC_5`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```

```{r}
# set threshold
edgePC = 6
edgePC_6 = edgePC
```

## Threshold: `r edgePC_6`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```


```{r}
# set threshold
edgePC = 7
edgePC_7 = edgePC
```

## Threshold: `r edgePC_7`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```

```{r}
# set threshold
edgePC = 8
edgePC_8 = edgePC
```

## Threshold: `r edgePC_8`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```

```{r}
# set threshold
edgePC = 9
edgePC_9 = edgePC
```

## Threshold: `r edgePC_9`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```


```{r}
# set threshold
edgePC = 10
edgePC_10 = edgePC
```

## Threshold: `r edgePC_10`%

```{r}
# plot groups
graph.data.threshold <- graph.data %>%
  filter(edgePC == !!edgePC) %>%
  dplyr::select(diag_group, !!metric) %>%
  mutate(!!metric_z := (.data[[!!metric]] - mean(.data[[!!metric]])) / sd(.data[[!!metric]]))

p.thresh <- ggplot(graph.data.threshold, aes_string(x = "diag_group", y = metric, fill = "diag_group")) +
  geom_boxplot() +
  labs(y = metric.name, x = "Diagnostic group") +
  theme(axis.text.x = element_text(angle = 90), legend.position = "None")

print(p.thresh + stat_compare_means(aes(label = paste0(..method.., ", p-value = ", ..p.format..)),
                                 method = "anova",
                                 paired = FALSE,
                                 # group.by = NULL,
                                 ref.group = NULL))
```


