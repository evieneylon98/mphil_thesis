```{r}
library(plyr)
library(dplyr)
library(dbplyr)
library(RSQLite)
library(DT)
library(ggplot2)
library(xtable)
library(rstan)
library(coda)
library(tidyr)
library(broom)
library(broom.mixed)
library(multcomp)
library(ggpubr)

options(mc.cores = parallel::detectCores())
rstan_options(auto_write = TRUE)
set.seed(1)

```

```{r}

# Load clinical scores

clin <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/clinical_scores.csv", header=T)
clin_no <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/clinical_scores_nooutliers.csv", header=T)
clin_break <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/clinicalscores_breakdown.csv", header=T)
clin_break_no <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/clinicalscores_breakdown_nooutliers.csv", header=T)
colnames(clin)[2]<-"session"
colnames(clin_no)[2]<-"session"
colnames(clin_break)[2]<-"session"
colnames(clin_break_no)[2]<-"session"

diagnosis.order = c("control",
                    "psp")
```

Mean Degree

```{r}
metric = "degree_wt"
metric_z = paste(metric, "z", sep = "_")
metric.name = "Mean Degree"

# Structural 

structural.db <- DBI::dbConnect(RSQLite::SQLite(), "/home/ern24/rds/hpc-work/data/graphResults/graphResults.db")

structural_deg.data = dbGetQuery(structural.db, sql(paste("SELECT DISTINCT ", metric, ".", metric, ", ",
                                            metric, ".node, ",
                                            metric, ".session_id, sessionData.subj, ",
                                            "sessionData.session, diagnosis.diag",
                                     " FROM sessionData",
                                     " LEFT JOIN ", metric, ", diagnosis ON sessionData.session_id = ", metric, ".session_id",
                                                                   " AND sessionData.subj = diagnosis.subj",
                                     " WHERE (diagnosis.diag = 'control' ",
                                     "OR diagnosis.diag = 'psp') ",
                                    sep = "")))

# structural_deg.data <- structural_deg.data %>%
#   mutate(!!metric := as.numeric(.data[[metric]]),
#          edgePC = as.numeric(edgePC),
#          diag = factor(diag, levels = diagnosis.order)) %>%
#   filter(is.finite(.data[[!!metric]]))

dbDisconnect(structural.db)

# Remove added nodes

structural_deg.data <- structural_deg.data[!(structural_deg.data$node==252 | structural_deg.data$node==251 | structural_deg.data$node==1 | structural_deg.data$node==2 | structural_deg.data$node==3 | structural_deg.data$node==4),]

# Mean by session_id
struct_dg <- subset(structural_deg.data, select = c(session_id, subj, session, diag))
struct_dg <- unique(struct_dg) 
structural_deg.data <- ddply(structural_deg.data, c("session_id", "subj", "session"), summarise, N=246, mean_degree_STRUCT=mean(degree_wt))
structural_deg.data <- merge(structural_deg.data, struct_dg)

# Set edge threshold
#structural_deg.data <- subset(structural_deg.data, edgePC == 8)

```

```{r}

# Functional

metric = "degree_wt"
metric_z = paste(metric, "z", sep = "_")
metric.name = "Mean Degree"

functional.db <- DBI::dbConnect(RSQLite::SQLite(), "/rds/project/jbr10/rds-jbr10-genfi/share_graphs/graphAnalysis/graphResults/graphResults.db")

functional_deg.data = dbGetQuery(functional.db, sql(paste("SELECT DISTINCT ", metric, ".", metric, ", ",
                                            metric, ".node, ",
                                            metric, ".session_id, sessionDataExcl.subj, sessionDataExcl.echo, ",
                                            "sessionDataExcl.session, diagnosis.diagnosis",
                                     " FROM sessionDataExcl",
                                     " LEFT JOIN ", metric, ", diagnosis ON sessionDataExcl.session_id = ", metric, ".session_id",
                                                                   " AND sessionDataExcl.subj = diagnosis.subj",
                                     " WHERE (diagnosis.diagnosis = 'Control' ",
                                     "OR diagnosis.diagnosis = 'Progressive Supranuclear Palsy (PSP)') ",
                                    sep = "")))

functional_deg.data$diagnosis[functional_deg.data$diagnosis == "Control"] <- "control"
functional_deg.data$diagnosis[functional_deg.data$diagnosis == "Progressive Supranuclear Palsy (PSP)"] <- "psp"

names(functional_deg.data)[names(functional_deg.data) == 'diagnosis'] <- 'diag'

# functional_deg.data <- functional_deg.data %>%
#   mutate(!!metric := as.numeric(.data[[metric]]),
#          edgePC = as.numeric(edgePC),
#          diag = factor(diag, levels = diagnosis.order)) %>%
#   filter(is.finite(.data[[!!metric]]))

dbDisconnect(functional.db)

# Mean by session_id
func_dg <- subset(functional_deg.data, select = c(session_id, subj, session, diag, echo))
func_dg <- unique(func_dg) 
functional_deg.data <- ddply(functional_deg.data, c("session_id", "subj", "session", "echo"), summarise, N=246, mean_degree_FUNC=mean(degree_wt))
functional_deg.data <- merge(functional_deg.data, func_dg)

# Set edge threshold
#functional_deg.data <- subset(functional_deg.data, edgePC == 8)

structural_deg.data <- subset(structural_deg.data, select = -c(session_id))
functional_deg.data <- subset(functional_deg.data, select = -c(session_id))
degree.data <- merge(structural_deg.data, functional_deg.data)

nuisance <- read.csv("/home/ern24/rds/hpc-work/data/DTIprocessed/nuisance_variables.csv")
    nuisance<- subset(nuisance, select=-c(diag_group))
    colnames(nuisance)[1]<-"subj"
    colnames(nuisance)[2]<-"session"
    
degree.data<- merge(degree.data, nuisance)

```

```{r}
# Plot S-F correlation for each subtype.

p.degree <- ggplot(data=degree.data, mapping=aes(x=mean_degree_STRUCT, y=mean_degree_FUNC)) + geom_point()

print(p.degree + facet_wrap( ~ diag) + stat_smooth(method = lm))

# Fit linear regression to each

options(contrasts=c("contr.sum", "contr.poly"))
con.degree <-subset(degree.data, diag == "control")
model.con.degree<-lm(mean_degree_FUNC ~ mean_degree_STRUCT + ageAtScan, data = con.degree)
psp.degree <-subset(degree.data, diag == "psp")
model.psp.degree<-lm(mean_degree_FUNC ~ mean_degree_STRUCT + ageAtScan, data = psp.degree)

print(summary(model.con.degree))
print(summary(model.psp.degree))
```

```{r}
# Remove motion outliers

degree.data_nooutliers <- degree.data[!(degree.data$subj=="sub-13672" |
                                      degree.data$subj=="sub-16257" |
                                      degree.data$subj=="sub-16486" |
                                        degree.data$subj=="sub-18735" |
                                        degree.data$subj=="sub-18829" |
                                        degree.data$subj=="sub-19411" & degree.data$session=="ses-20110406" |
                                        degree.data$subj=="sub-22076" & degree.data$session=="ses-20141001" |
                                        degree.data$subj=="sub-23419" |
                                        degree.data$subj=="sub-23977" |
                                        degree.data$subj=="sub-24033" |
                                        degree.data$subj=="sub-24395" |
                                        degree.data$subj=="sub-24600" |
                                        degree.data$subj=="sub-18281" |
                                        degree.data$subj=="sub-21490"),]

# Plot S-F correlation for each subtype.

p.degree_nooutliers <- ggplot(data=degree.data_nooutliers, mapping=aes(x=mean_degree_STRUCT, y=mean_degree_FUNC)) + geom_point() + stat_smooth(method=lm)

print(p.degree_nooutliers + facet_wrap( ~ diag) + stat_cor(method="spearman"))

options(contrasts=c("contr.treatment", "contr.poly"))
con.degree_no <-subset(degree.data_nooutliers, diag == "control")
model.con.degree_no<-lm(mean_degree_FUNC ~ mean_degree_STRUCT + ageAtScan, data = con.degree_no)
psp.degree_no <-subset(degree.data_nooutliers, diag == "psp")
psp.degree_nooutliers_clin <- merge(psp.degree_no, clin_no)
psp.degree_nooutliers_clin_break <- merge(psp.degree_no, clin_break_no)
model.psp.degree_no<-lm(mean_degree_FUNC ~ mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin)

print(summary(model.con.degree_no))
print(summary(model.psp.degree_no))
```

```{r}
# Plot correlation with clinical scores for PSP.


p.stdegree_nooutliers_psprs <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_STRUCT, y=PSPRSTotal)) + geom_point() + stat_smooth(method=lm)
print(p.stdegree_nooutliers_psprs + stat_cor(method="spearman"))
p.fcdegree_nooutliers_psprs <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_FUNC, y=PSPRSTotal)) + geom_point() + stat_smooth(method=lm)
print(p.fcdegree_nooutliers_psprs + stat_cor(method="spearman"))
model.st.psprs<-lm(PSPRSTotal~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin)
model.fc.psprs<-lm(PSPRSTotal~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin)
print(summary(model.st.psprs))
print(summary(model.fc.psprs))

p.stdegree_nooutliers_acer <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_STRUCT, y=ACERTotal)) + geom_point() + stat_smooth(method=lm)
print(p.stdegree_nooutliers_acer + stat_cor(method="spearman"))
p.fcdegree_nooutliers_acer <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_FUNC, y=ACERTotal)) + geom_point() + stat_smooth(method=lm)
print(p.fcdegree_nooutliers_acer + stat_cor(method="spearman"))
model.st.ACER<-lm(ACERTotal~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin)
model.fc.ACER<-lm(ACERTotal~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin)
print(summary(model.st.ACER))
print(summary(model.fc.ACER))

p.stdegree_nooutliers_fab <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_STRUCT, y=FABTotal)) + geom_point() + stat_smooth(method=lm)
print(p.stdegree_nooutliers_fab + stat_cor(method="spearman"))
p.fcdegree_nooutliers_fab <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_FUNC, y=FABTotal)) + geom_point() + stat_smooth(method=lm)
print(p.fcdegree_nooutliers_fab + stat_cor(method="spearman"))
model.st.FAB<-lm(FABTotal~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin)
model.fc.FAB<-lm(FABTotal~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin)
print(summary(model.st.FAB))
print(summary(model.fc.FAB))

p.stdegree_nooutliers_updrs <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_STRUCT, y=UPDRSTotal)) + geom_point() + stat_smooth(method=lm)
print(p.stdegree_nooutliers_updrs + stat_cor(method="spearman"))
p.fcdegree_nooutliers_updrs <- ggplot(data=psp.degree_nooutliers_clin, mapping=aes(x=mean_degree_FUNC, y=UPDRSTotal)) + geom_point() + stat_smooth(method=lm)
print(p.fcdegree_nooutliers_updrs + stat_cor(method="spearman"))
model.st.UPDRS<-lm(UPDRSTotal~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin)
model.fc.UPDRS<-lm(UPDRSTotal~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin)
print(summary(model.st.UPDRS))
print(summary(model.fc.UPDRS))

```

```{r}
# And subscores

p.st_pspmen <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=psp_37_mentot)) + geom_point() + stat_smooth(method=lm)
print(p.st_pspmen + stat_cor(method="spearman"))
p.fc_pspmen <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=psp_37_mentot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_pspmen + stat_cor(method="spearman"))
model.st.pspmen<-lm(psp_37_mentot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.pspmen<-lm(psp_37_mentot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.pspmen))
print(summary(model.fc.pspmen))

p.st_pspbul <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=psp_38_bultot)) + geom_point() + stat_smooth(method=lm)
print(p.st_pspbul + stat_cor(method="spearman"))
p.fc_pspbul <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=psp_38_bultot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_pspbul + stat_cor(method="spearman"))
model.st.pspbul<-lm(psp_38_bultot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.pspbul<-lm(psp_38_bultot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.pspbul))
print(summary(model.fc.pspbul))

p.st_pspocu <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=psp_39_ocutot)) + geom_point() + stat_smooth(method=lm)
print(p.st_pspocu + stat_cor(method="spearman"))
p.fc_pspocu <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=psp_39_ocutot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_pspocu + stat_cor(method="spearman"))
model.st.pspocu<-lm(psp_39_ocutot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.pspocu<-lm(psp_39_ocutot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.pspocu))
print(summary(model.fc.pspocu))

p.st_psplimb <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=psp_40_limtot)) + geom_point() + stat_smooth(method=lm)
print(p.st_psplimb + stat_cor(method="spearman"))
p.fc_psplimb <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=psp_40_limtot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_psplimb + stat_cor(method="spearman"))
model.st.psplimb<-lm(psp_40_limtot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.psplimb<-lm(psp_40_limtot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.psplimb))
print(summary(model.fc.psplimb))

p.st_pspgait <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=psp_41_gaittot)) + geom_point() + stat_smooth(method=lm)
print(p.st_pspgait + stat_cor(method="spearman"))
p.fc_pspgait <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=psp_41_gaittot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_pspgait + stat_cor(method="spearman"))
model.st.pspgait<-lm(psp_41_gaittot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.pspgait<-lm(psp_41_gaittot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.pspgait))
print(summary(model.fc.pspgait))

p.st_aceratt <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=acer_38_atttot)) + geom_point() + stat_smooth(method=lm)
print(p.st_aceratt + stat_cor(method="spearman"))
p.fc_aceratt <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=acer_38_atttot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_aceratt + stat_cor(method="spearman"))
model.st.aceratt<-lm(acer_38_atttot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.aceratt<-lm(acer_38_atttot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.aceratt))
print(summary(model.fc.aceratt))

p.st_acermem <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=acer_39_memtot)) + geom_point() + stat_smooth(method=lm)
print(p.st_acermem + stat_cor(method="spearman"))
p.fc_acermem <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=acer_39_memtot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_acermem + stat_cor(method="spearman"))
model.st.acermem<-lm(acer_39_memtot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.acermem<-lm(acer_39_memtot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.acermem))
print(summary(model.fc.acermem))

p.st_acerlang <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=acer_41_langtot)) + geom_point() + stat_smooth(method=lm)
print(p.st_acerlang + stat_cor(method="spearman"))
p.fc_acerlang <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=acer_41_langtot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_acerlang + stat_cor(method="spearman"))
model.st.acerlang<-lm(acer_41_langtot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.acerlang<-lm(acer_41_langtot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.acerlang))
print(summary(model.fc.acerlang))

p.st_acerflu <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=acer_40_fluentot)) + geom_point() + stat_smooth(method=lm)
print(p.st_acerflu + stat_cor(method="spearman"))
p.fc_acerflu <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=acer_40_fluentot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_acerflu + stat_cor(method="spearman"))
model.st.acerflu<-lm(acer_40_fluentot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.acerflu<-lm(acer_40_fluentot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.acerflu))
print(summary(model.fc.acerflu))

p.st_acervis <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_STRUCT, y=acer_42_visuosptot)) + geom_point() + stat_smooth(method=lm)
print(p.st_acervis + stat_cor(method="spearman"))
p.fc_acervis <- ggplot(data=psp.degree_nooutliers_clin_break, mapping=aes(x=mean_degree_FUNC, y=acer_42_visuosptot)) + geom_point() + stat_smooth(method=lm)
print(p.fc_acervis + stat_cor(method="spearman"))
model.st.acervis<-lm(acer_42_visuosptot~mean_degree_STRUCT + ageAtScan, data = psp.degree_nooutliers_clin_break)
model.fc.acervis<-lm(acer_42_visuosptot~mean_degree_FUNC + ageAtScan, data = psp.degree_nooutliers_clin_break)
print(summary(model.st.acervis))
print(summary(model.fc.acervis))


```

```{r}
# Mediation Analysis

# step 1: test for total effect 

fit.totaleffect_psprs=lm(PSPRSTotal~mean_degree_STRUCT,degree.data_nooutliers_clin)
print(summary(fit.totaleffect_psprs))

fit.totaleffect_acer=lm(ACERTotal~mean_degree_STRUCT,degree.data_nooutliers_clin)
print(summary(fit.totaleffect_acer))

fit.totaleffect_fab=lm(FABTotal~mean_degree_STRUCT,degree.data_nooutliers_clin)
print(summary(fit.totaleffect_fab))

fit.totaleffect_updrs=lm(UPDRSTotal~mean_degree_STRUCT,degree.data_nooutliers_clin)
print(summary(fit.totaleffect_updrs))
```

```{r}

# step 2: effect of structure on function. This is a prerequesite for mediation being possible.

fit.mediator=lm(mean_degree_FUNC~mean_degree_STRUCT,degree.data_nooutliers_clin)
print(summary(fit.mediator))
```

```{r}
# step 3: simultaneously test effect of structure and function on clinical.

fit.psprs=lm(PSPRSTotal~mean_degree_STRUCT+mean_degree_FUNC,degree.data_nooutliers_clin)
print(summary(fit.psprs))

fit.acer=lm(ACERTotal~mean_degree_STRUCT+mean_degree_FUNC,degree.data_nooutliers_clin)
print(summary(fit.acer))

fit.fab=lm(FABTotal~mean_degree_STRUCT+mean_degree_FUNC,degree.data_nooutliers_clin)
print(summary(fit.fab))

fit.updrs=lm(UPDRSTotal~mean_degree_STRUCT+mean_degree_FUNC,degree.data_nooutliers_clin)
print(summary(fit.updrs))
```

```{r}
# step 4: compare direct and indirect effect

library(mediation)

results_psprs = mediate(fit.mediator, fit.psprs, treat='mean_degree_STRUCT', mediator='mean_degree_FUNC', boot=T)
print(summary(results_psprs))

results_acer = mediate(fit.mediator, fit.acer, treat='mean_degree_STRUCT', mediator='mean_degree_FUNC', boot=T)
print(summary(results_acer))

results_fab = mediate(fit.mediator, fit.fab, treat='mean_degree_STRUCT', mediator='mean_degree_FUNC', boot=T)
print(summary(results_fab))

results_updrs = mediate(fit.mediator, fit.updrs, treat='mean_degree_STRUCT', mediator='mean_degree_FUNC', boot=T)
print(summary(results_updrs))
```
